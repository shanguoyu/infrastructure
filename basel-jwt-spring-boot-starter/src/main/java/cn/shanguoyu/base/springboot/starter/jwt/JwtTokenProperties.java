package cn.shanguoyu.base.springboot.starter.jwt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @description:jwt相关过期配置
 * @author：sgy
 * @date: 2023-03-19
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "base.jwt")
public class JwtTokenProperties {
    /**
     * 令牌有效期（默认30分钟）
     */
    private int expireTime = 30;
    /**
     * 令牌秘钥
     */
    private String secret;
    /**
     * 刷新令牌有效时间默认是访问令牌的2倍
     */
    private int refreshTime = expireTime * 2;
    /**
     * 小程序令牌有效期（默认15天）
     */
    private int wxExpireTime = 15;


    /**
     * 是否允许账户多终端同时登录（true允许 false不允许）
     */
//    private boolean soloLogin;
}
