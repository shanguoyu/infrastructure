package cn.shanguoyu.base.springboot.starter.jwt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:实体类
 * @author：sgy
 * @date: 2023-03-19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Token {
    /**
     * 访问token
     */
    private String accessToken;

    /**
     * 刷新token
     */
    private String refreshToken;
    /**
     * 访问令牌过期时间戳
     */
    private long accessTokenExpireTime;
}
