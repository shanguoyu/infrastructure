package cn.shanguoyu.base.springboot.starter.jwt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:创建令牌时候的参数
 * @author：sgy
 * @date: 2023-03-19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateTokenParam {
    /**
     * 要存储在JWT中的自定义数据
     */
    private Object data;
    /**
     * 是否长期有效
     */
    private boolean longTerm;

}
