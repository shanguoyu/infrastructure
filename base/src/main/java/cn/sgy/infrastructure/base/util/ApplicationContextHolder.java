package cn.sgy.infrastructure.base.util;

import org.springframework.context.ApplicationContext;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * @description Spring容器上下文，把Application从动态注入变为静态使用，方便操作
 * @author sgy
 * @date 2023-02-28
 */
public class ApplicationContextHolder {
    /**
     * Spring容器
     */
    private static ApplicationContext CONTEXT;

    public ApplicationContextHolder(ApplicationContext applicationContext) {
        CONTEXT = applicationContext;
    }
    /**
     * 根据Class获取Bean
     *
     * @param clazz 类型
     * @param <T>   泛型类型
     * @return Bean
     */
    public static <T> T getBean(Class<T> clazz) {
        return CONTEXT.getBean(clazz);
    }

    /**
     * 根据名称获取Bean并且验证对应Class类型
     *
     * @param name  BeanName
     * @param clazz 类型
     * @param <T>   泛型
     * @return Bean
     */
    public static <T> T getBean(String name, Class<T> clazz) {
        return CONTEXT.getBean(name, clazz);
    }

    /**
     * 获取多个Bean根据指定类型
     *
     * @param clazz 类型
     * @param <T>   泛型
     * @return Key->BeanName，Value->Bean
     */
    public static <T> Map<String, T> getBeansOfType(Class<T> clazz) {
        return CONTEXT.getBeansOfType(clazz);
    }

    /**
     * 查找注解Bean根据BeanName和注解类型
     *
     * @param beanName       BeanName
     * @param annotationType 注解类型
     * @param <A>            泛型继承自Annotation
     * @return Bean
     */
    public static <A extends Annotation> A findAnnotationOnBean(String beanName, Class<A> annotationType) {
        return CONTEXT.findAnnotationOnBean(beanName, annotationType);
    }

    /**
     * 获取Spring容器
     *
     * @return 容器
     */
    public static ApplicationContext getInstance() {
        return CONTEXT;
    }


}
