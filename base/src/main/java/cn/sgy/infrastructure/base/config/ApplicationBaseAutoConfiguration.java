package cn.sgy.infrastructure.base.config;

import cn.sgy.infrastructure.base.initevent.ApplicationContentPostProcessor;
import cn.sgy.infrastructure.base.util.ApplicationContextHolder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description 应用基础自动装配
 * @author sgy
 * @date 2023-02-28
 */
@Configuration
public class ApplicationBaseAutoConfiguration {
    /**
     * Sring容器上下文
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public ApplicationContextHolder congoApplicationContextHolder(ApplicationContext applicationContext) {
        return new ApplicationContextHolder(applicationContext);
    }

    /**
     * Spring初始化事件处理器，负责接收Spring容器初始化事件然后新发布一个事件
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public ApplicationContentPostProcessor congoApplicationContentPostProcessor(ApplicationContext applicationContext) {
        return new ApplicationContentPostProcessor(applicationContext);
    }
}
