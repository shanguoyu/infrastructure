package cn.sgy.infrastructure.base.initevent;

import org.springframework.context.ApplicationEvent;

/**
 * @description 应用初始化事件，规约事件，通过此事件可以查看业务系统所有初始化行为
 * @author sgy
 * @date 2023-02-28
 */
public class ApplicationInitializingEvent extends ApplicationEvent {
    public ApplicationInitializingEvent(Object source) {
        super(source);
    }
}
