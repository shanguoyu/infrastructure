/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.sgy.base.log.aspect;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.SystemClock;
import cn.sgy.base.log.annotation.MLog;
import com.alibaba.fastjson2.JSON;
import lombok.Data;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Optional;


/**
 * @description 日志打印 AOP 切面
 * @author  sgy
 * @date 2023-03-11
 */
@Aspect
public class MLogPrintAspect {
    
    /**
     * 打印类或方法上的 {@link MLog}
     */
    @Around("@within(cn.sgy.base.log.annotation.MLog) || @annotation(cn.sgy.base.log.annotation.MLog)")
    public Object printMLog(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = SystemClock.now();
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Logger log = LoggerFactory.getLogger(methodSignature.getDeclaringType());
        String beginTime = DateUtil.now();
        Object result = null;
        try {
            result = joinPoint.proceed();
        } finally {
            Method targetMethod = joinPoint.getTarget().getClass().getDeclaredMethod(methodSignature.getName(), methodSignature.getMethod().getParameterTypes());
            MLog logAnnotation = Optional.ofNullable(targetMethod.getAnnotation(MLog.class)).orElse(joinPoint.getTarget().getClass().getAnnotation(MLog.class));
            if (logAnnotation != null) {
                MLogPrint logPrint = new MLogPrint();
                logPrint.setBeginTime(beginTime);
                if (logAnnotation.input()) {
                    logPrint.setInputParams(buildInput(joinPoint));
                }
                if (logAnnotation.output()) {
                    logPrint.setOutputParams(result);
                }
                String methodType = "", requestURI = "";
                try {
                    ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                    methodType = servletRequestAttributes.getRequest().getMethod();
                    requestURI = servletRequestAttributes.getRequest().getRequestURI();
                } catch (Exception ignored) {
                }
                log.info("[{}] {}, executeTime: {}ms, info: {}", methodType, requestURI, SystemClock.now() - startTime, JSON.toJSONString(logPrint));
            }
        }
        return result;
    }
    /**
     * 打印类或方法上的 {@link MLog}
     */
//    @Around("@within(cn.sgy.base.log.annotation.MLog) || @annotation(cn.sgy.base.log.annotation.MLog)")
//    public Object printMLog(ProceedingJoinPoint joinPoint) throws Throwable {
//        long startTime = SystemClock.now();
//        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
//        Logger log = LoggerFactory.getLogger(methodSignature.getDeclaringType());
//        String beginTime = DateUtil.now();
//        Object result = null;
//        MLog mLog = getMLog(joinPoint);
//        printInput(mLog,joinPoint,log);
//        try {
//            result = joinPoint.proceed();
//        } finally {
//            Method targetMethod = joinPoint.getTarget().getClass().getDeclaredMethod(methodSignature.getName(), methodSignature.getMethod().getParameterTypes());
//            MLog logAnnotation = Optional.ofNullable(targetMethod.getAnnotation(MLog.class)).orElse(joinPoint.getTarget().getClass().getAnnotation(MLog.class));
//            if (logAnnotation != null) {
//                MLogPrint logPrint = new MLogPrint();
//                logPrint.setBeginTime(beginTime);
//                if (logAnnotation.input()) {
//                    logPrint.setInputParams(buildInput(joinPoint));
//                }
//                if (logAnnotation.output()) {
//                    logPrint.setOutputParams(result);
//                }
//                String methodType = "", requestURI = "";
//                try {
//                    ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//                    methodType = servletRequestAttributes.getRequest().getMethod();
//                    requestURI = servletRequestAttributes.getRequest().getRequestURI();
//                } catch (Exception ignored) {
//                }
//                log.info("[{}] {}, executeTime: {}ms, info: {}", methodType, requestURI, SystemClock.now() - startTime, JSON.toJSONString(logPrint));
//            }
//        }
//        return result;
//    }

    /**
     * 获取MLog注解
     * @param joinPoint
     * @return
     */
    private MLog getMLog(ProceedingJoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method targetMethod = null;
        try {
            targetMethod = joinPoint.getTarget().getClass().getDeclaredMethod(methodSignature.getName(), methodSignature.getMethod().getParameterTypes());
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        MLog logAnnotation = Optional.ofNullable(targetMethod.getAnnotation(MLog.class)).orElse(joinPoint.getTarget().getClass().getAnnotation(MLog.class));
        return logAnnotation;
    }

    /**
     * 打印入参
     */
    private void printInput(MLog mLog,ProceedingJoinPoint joinPoint,Logger logger) {
        if (mLog!=null && mLog.input()){
            String beginTime = DateUtil.now();
            MLogPrint logPrint = new MLogPrint();
            logPrint.setBeginTime(beginTime);
            logPrint.setInputParams(buildInput(joinPoint));
        }
    }

    private Object[] buildInput(ProceedingJoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Object[] printArgs = new Object[args.length];
        for (int i = 0; i < args.length; i++) {
            if ((args[i] instanceof HttpServletRequest) || args[i] instanceof HttpServletResponse) {
                continue;
            }
            if (args[i] instanceof byte[]) {
                printArgs[i] = "byte array";
            } else if (args[i] instanceof MultipartFile) {
                printArgs[i] = "file";
            } else {
                printArgs[i] = args[i];
            }
        }
        return printArgs;
    }
    
    @Data
    private class MLogPrint {
        
        private String beginTime;
        
        private Object[] inputParams;
        
        private Object outputParams;
    }
}
