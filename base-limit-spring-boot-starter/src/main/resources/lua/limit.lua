local c
c = redis.call('get',KEYS[1]) -- 获取限流Key对应的value，也就是访问次数
-- 如果限流key对应的value已经大于了最大访问次数则返回目前的限流次数
if c and tonumber(c) > tonumber(ARGV[1]) then
    return c;
end
-- 执行计算器自加，限流次数+1
c = redis.call('incr',KEYS[1])
if tonumber(c) == 1 then
-- 从第一次调用开始限流，设置对应键值的过期
    redis.call('expire',KEYS[1],ARGV[2])
end
return c;