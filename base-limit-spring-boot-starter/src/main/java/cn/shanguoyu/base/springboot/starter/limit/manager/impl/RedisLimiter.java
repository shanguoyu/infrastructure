package cn.shanguoyu.base.springboot.starter.limit.manager.impl;

import cn.shanguoyu.base.springboot.starter.cache.abs.Cache;
import cn.shanguoyu.base.springboot.starter.limit.manager.abs.AbstractLimiterManager;
import cn.shanguoyu.base.springboot.starter.limit.manager.pojo.Limiter;
import com.google.common.collect.ImmutableList;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.script.DefaultRedisScript;

/**
 * @description:Redis+Lua计数器方式限流
 * @author：sgy
 * @date: 2023-05-30
 */
@Slf4j
@AllArgsConstructor
public class RedisLimiter extends AbstractLimiterManager {
    private final Cache cache;
    private final DefaultRedisScript<Long> limitScript;

    /**
     * 用Redis+Lua实现分布式限流算法是计数器实现思路如下：
     * - 获取period与limit与prefix属性
     * - 组装参数执行Lua脚本
     * - 返回结果
     *
     * @param limiter
     * @return
     */
    @Override
    public boolean limit(Limiter limiter) {
        // 获取各种属性
        int period = limiter.getPeriod();
        int limit = limiter.getLimit();
        String key = limiter.getKey();
        // 拼装参数执行Lua脚本
        ImmutableList<String> keys = ImmutableList.of(key);
        Long count = cache.executeLua(limitScript, keys, limit, period);
        // 判断是正常还是异常请求
        if (count.intValue() > limit) {
            return false;
        }
        return true;
    }
}
