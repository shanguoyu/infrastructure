package cn.shanguoyu.base.springboot.starter.limit.aop;

import cn.shanguoyu.base.springboot.starter.limit.enums.LimitType;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @description:限流注解
 * @author：sgy
 * @date: 2023-05-28
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RateLimiter {
    /**
     * 限流key
     *
     * @return
     */
    String key() default "rate:limit:";

    /**
     * 每秒钟最多少个请求，数据必须大于0，给单机限流用的，是令牌桶算法，下面那两个是给计数器算法的分布式限流用的
     **/
    double permitsPerSecond() default 10;

    /**
     * 给定的时间段 单位秒
     */
    int period() default 60;

    /**
     * 最多的访问限制次数
     */
    int limit() default 10;

    /**
     * 限流类型
     */
    LimitType limitType() default LimitType.GLOBAL;

    /**
     * 请求等待时间
     **/
    int timeout() default 5;

    /**
     * 请求等待时间单位
     **/
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * Key的prefix
     *
     * @return String
     */
    String prefix() default "";
}
