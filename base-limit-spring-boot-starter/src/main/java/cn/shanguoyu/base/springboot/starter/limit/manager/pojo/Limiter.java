package cn.shanguoyu.base.springboot.starter.limit.manager.pojo;

import cn.shanguoyu.base.springboot.starter.limit.enums.LimitType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * @description:限流信息的实体
 * @author：sgy
 * @date: 2023-05-29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Limiter {
    /**
     * 限流key
     * 全局限流：限流key本身
     * IP限流：限流key:ip，这个IP不需要用户指定，这个模块内部会自己添加
     */
    private String key;
    /**
     * 每秒钟最多少个请求
     **/
    private double permitsPerSecond;
    /**
     * 给定的时间段 单位秒
     */
    private int period;
    /**
     * 最多的访问限制次数
     */
    private int limit;
    /**
     * 请求等待时间
     **/
    private int timeout;
    /**
     * 请求等待时间单位
     **/
    private TimeUnit timeUnit;
    /**
     * 请求限流类型
     */
    private LimitType limitType;
    /**
     * Key的prefix redis前缀
     */
    private String prefix;
}
