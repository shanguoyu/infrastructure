package cn.shanguoyu.base.springboot.starter.limit.manager.impl;

import cn.shanguoyu.base.springboot.starter.limit.manager.abs.AbstractLimiterManager;
import cn.shanguoyu.base.springboot.starter.limit.manager.pojo.Limiter;
import com.google.common.util.concurrent.RateLimiter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description:Guava的限流器器
 * @author：sgy
 * @date: 2023-05-30
 */
@Slf4j
public class GuavaLimiter extends AbstractLimiterManager {
    /**
     * 不同的接口，不同的流量控制
     * map的key为 Limiter.key
     */
    private volatile Map<String, RateLimiter> limitMap = new ConcurrentHashMap<>();

    @Override
    public boolean limit(Limiter limiter) {
        // key作用：不同的接口，不同的流量控制
        String key = limiter.getKey();
        RateLimiter rateLimiter;
        // 如果不存在，也就是被第一次访问，双重检查锁
        if (!limitMap.containsKey(key)) {
            synchronized (GuavaLimiter.class) {
                if (!limitMap.containsKey(key)) {
                    // 创建令牌桶
                    rateLimiter = RateLimiter.create(limiter.getPermitsPerSecond());
                    // 添加到缓存中
                    limitMap.put(key, rateLimiter);
                }
            }
        }
        // 获取RateLimiter
        rateLimiter = limitMap.get(key);
        // 拿令牌
        return rateLimiter.tryAcquire(limiter.getTimeout(), limiter.getTimeUnit());
    }
}
