package cn.shanguoyu.base.springboot.starter.limit.manager.abs;

import cn.shanguoyu.base.springboot.starter.limit.manager.pojo.Limiter;

/**
 * @description:限流管理器：负责执行限流逻辑返回能否访问
 * @author：sgy
 * @date: 2023-05-29
 */
public interface LimiterManager {

    /**
     * 能否访问
     *
     * @return true就是能访问，false就是不能访问
     **/
    boolean tryAccess(Limiter limiter);
}
