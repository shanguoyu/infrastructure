package cn.shanguoyu.base.springboot.starter.limit.enums;

/**
 * @description:请求限流类型
 * @author：sgy
 * @date: 2023-05-28
 */
public enum LimitType {
    /**
     * 默认策略全局限流
     */
    GLOBAL,
    /**
     * 根据请求者IP进行限流
     */
    IP
}
