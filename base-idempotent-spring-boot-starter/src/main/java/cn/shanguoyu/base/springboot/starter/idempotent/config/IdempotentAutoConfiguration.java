/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.shanguoyu.base.springboot.starter.idempotent.config;

import cn.shanguoyu.base.springboot.starter.cache.abs.Cache;
import cn.shanguoyu.base.springboot.starter.idempotent.aop.IdempotentAspect;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.param.IdempotentParamExecuteHandler;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.param.IdempotentParamService;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token.IdempotentTokenController;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token.IdempotentTokenExecuteHandler;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token.IdempotentTokenProperties;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token.IdempotentTokenService;
import org.redisson.api.RedissonClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @description:幂等自动装配
 * @author：sgy
 * @date: 2023-03-10
 */
@EnableConfigurationProperties(IdempotentTokenProperties.class)
public class IdempotentAutoConfiguration {

    /**
     * 幂等切面
     */
    @Bean
    public IdempotentAspect idempotentAspect() {
        return new IdempotentAspect();
    }

    /**
     * 参数方式幂等实现，基于 RestAPI 场景
     */
    @Bean
    @ConditionalOnMissingBean
    public IdempotentParamService idempotentParamExecuteHandler(RedissonClient redissonClient) {
        return new IdempotentParamExecuteHandler(redissonClient);
    }

    /**
     * Token 方式幂等实现，基于 RestAPI 场景
     */
    @Bean
    @ConditionalOnMissingBean
    public IdempotentTokenService idempotentTokenExecuteHandler(Cache cache,
                                                                IdempotentTokenProperties idempotentProperties) {
        return new IdempotentTokenExecuteHandler(cache, idempotentProperties);
    }

    /**
     * 申请幂等 Token 控制器，基于 RestAPI 场景
     */
    @Bean
    public IdempotentTokenController idempotentTokenController(IdempotentTokenService idempotentTokenService) {
        return new IdempotentTokenController(idempotentTokenService);
    }

}
