package cn.shanguoyu.base.springboot.starter.idempotent.core.factory;

import cn.shanguoyu.base.springboot.starter.base.ApplicationContextHolder;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.IdempotentExecuteHandler;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.param.IdempotentParamService;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.spel.IdempotentSpELByMQExecuteHandler;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.spel.IdempotentSpELByRestAPIExecuteHandler;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token.IdempotentTokenService;
import cn.shanguoyu.base.springboot.starter.idempotent.enums.IdempotentSceneEnum;
import cn.shanguoyu.base.springboot.starter.idempotent.enums.IdempotentTypeEnum;

/**
 * @description:幂等执行处理器工厂
 * @author：sgy
 * @date: 2023-06-03
 */
public class IdempotentExecuteHandlerFactory {
    private IdempotentExecuteHandlerFactory() {

    }

    /**
     * 简单工厂模式，根据条件返回对应的实体
     * @param scene
     * @param type
     * @return
     */
    public static IdempotentExecuteHandler getInstance(IdempotentSceneEnum scene, IdempotentTypeEnum type) {
        IdempotentExecuteHandler result = null;
        switch (scene) {
            case RESTAPI:
                switch (type) {
                    case PARAM:
                        result = ApplicationContextHolder.getBean(IdempotentParamService.class);
                        break;
                    case TOKEN:
                        result = ApplicationContextHolder.getBean(IdempotentTokenService.class);
                        break;
                    case SPEL:
                        result = ApplicationContextHolder.getBean(IdempotentSpELByRestAPIExecuteHandler.class);
                        break;
                }
                break;
            case MQ:
                result = ApplicationContextHolder.getBean(IdempotentSpELByMQExecuteHandler.class);
                break;
            default:
                break;
        }
        return result;
    }
}
