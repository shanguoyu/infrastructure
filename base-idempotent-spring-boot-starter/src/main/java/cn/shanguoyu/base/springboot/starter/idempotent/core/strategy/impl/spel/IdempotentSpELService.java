package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.spel;

import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.IdempotentExecuteHandler;

/**
 * @description:SpEL 方式幂等实现接口
 * @author：sgy
 * @date: 2023-06-04
 */
public interface IdempotentSpELService extends IdempotentExecuteHandler {
}
