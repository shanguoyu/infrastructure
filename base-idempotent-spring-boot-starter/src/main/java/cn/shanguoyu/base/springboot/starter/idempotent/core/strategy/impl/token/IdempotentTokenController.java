package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token;

import cn.shanguoyu.base.springboot.starter.convention.result.Result;
import cn.shanguoyu.base.springboot.starter.convention.result.Results;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:获得幂等Token的控制器
 * @author：sgy
 * @date: 2023-06-03
 */
@RestController
@RequiredArgsConstructor
public class IdempotentTokenController {
    private final IdempotentTokenService idempotentTokenService;

    /**
     * 请求幂等Token
     *
     * @return
     */
    @GetMapping("/idempotent/token")
    public Result<String> createToken() {
        return Results.success(idempotentTokenService.createToken());
    }

}
