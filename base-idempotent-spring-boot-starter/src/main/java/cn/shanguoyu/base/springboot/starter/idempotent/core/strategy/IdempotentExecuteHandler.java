package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy;

import cn.shanguoyu.base.springboot.starter.idempotent.aop.Idempotent;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @description:幂等策略顶层接口
 * @author：sgy
 * @date: 2023-06-03
 */
public interface IdempotentExecuteHandler {
    /**
     * 执行幂等逻辑，模板模式的顶层方法
     *
     * @param joinPoint
     * @param idempotent
     */
    void execute(ProceedingJoinPoint joinPoint, Idempotent idempotent);

    /**
     * 异常流程处理
     */
    default void exceptionProcessing() {

    }
    /**
     * 后置处理，在业务操作结束后进行扫尾操作
     */
    default void postProcessing(){

    }
}
