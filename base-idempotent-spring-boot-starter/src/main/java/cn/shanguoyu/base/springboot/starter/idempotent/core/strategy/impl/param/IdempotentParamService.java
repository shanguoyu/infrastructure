package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.param;

import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.IdempotentExecuteHandler;

/**
 * @description:参数方式(分布式锁)幂等实现接口
 * @author：sgy
 * @date: 2023-06-04
 */
public interface IdempotentParamService extends IdempotentExecuteHandler {
}
