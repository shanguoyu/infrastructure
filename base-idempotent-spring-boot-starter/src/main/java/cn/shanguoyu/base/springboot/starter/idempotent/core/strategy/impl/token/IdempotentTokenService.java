package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token;

import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.IdempotentExecuteHandler;

/**
 * @description:
 * @author：sgy
 * @date: 2023-06-03
 */
public interface IdempotentTokenService extends IdempotentExecuteHandler {
    /**
     * 创建幂等Key
     * @return
     */
    String createToken();
}
