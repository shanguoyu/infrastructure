package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.token;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @description:幂等属性配置，注解上对于幂等的配置是不行的，因为你调用Controller生成Token的时候很难拿到注解的配置信息
 * @author：sgy
 * @date: 2023-06-03
 */
@Data
@ConfigurationProperties(prefix = IdempotentTokenProperties.PREFIX)
public class IdempotentTokenProperties {
    public static final String PREFIX = "base.idempotent.token";

    /**
     * Token 幂等 Key 前缀
     */
    private String prefix;
    /**
     * Token 申请后过期时间，默认1小时
     */
    private long timeout = 3600L;
}
