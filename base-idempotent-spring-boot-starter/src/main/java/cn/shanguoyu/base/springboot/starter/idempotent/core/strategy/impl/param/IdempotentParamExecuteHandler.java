package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.impl.param;

import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.shanguoyu.base.springboot.starter.convention.exception.ClientException;
import cn.shanguoyu.base.springboot.starter.idempotent.core.IdempotentParamWrapper;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.IdempotentContext;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.abs.AbstractIdempotentTemplate;
import cn.shanguoyu.base.springboot.starter.web.toolkit.MvcUtil;
import com.alibaba.fastjson2.JSON;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

/**
 * @description:参数实现分布式锁的幂等方案
 * @author：sgy
 * @date: 2023-06-04
 */
@RequiredArgsConstructor
public class IdempotentParamExecuteHandler extends AbstractIdempotentTemplate implements IdempotentParamService {
    private final RedissonClient redissonClient;
    private final static String LOCK = "lock:param:restAPI";

    @Override
    protected void buildWrapper(IdempotentParamWrapper idempotentParamWrapper) {
        String lockKey = String.format("idempotent:path:%s:currentUserId:%s:md5:%s", MvcUtil.getHttpRequest().getServletPath(), null, calcArgsMD5(idempotentParamWrapper.getJoinPoint()));
        if (StrUtil.isNotBlank(idempotentParamWrapper.getIdempotent().uniqueKeyPrefix())) {
            lockKey = StrUtil.join(idempotentParamWrapper.getIdempotent().uniqueKeyPrefix(), StrPool.COLON, lockKey);
        }
        idempotentParamWrapper.setLockKey(lockKey);
    }

    @Override
    protected void handler(IdempotentParamWrapper wrapper) {
        String lockKey = wrapper.getLockKey();
        RLock lock = redissonClient.getLock(lockKey);
        if (!lock.tryLock()) {
            throw new ClientException(wrapper.getIdempotent().message());
        }
        IdempotentContext.put(LOCK, lock);
    }

    /**
     * 对方法所有参数进行MD5
     *
     * @return joinPoint md5
     */
    private String calcArgsMD5(ProceedingJoinPoint joinPoint) {
        return DigestUtil.md5Hex(JSON.toJSONBytes(joinPoint.getArgs()));
    }

    @Override
    public void postProcessing() {
        RLock lock = null;
        try {
            lock = (RLock) IdempotentContext.getKey(LOCK);
        } finally {
            if (lock != null && lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }
}
