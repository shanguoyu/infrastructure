package cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.abs;


import cn.shanguoyu.base.springboot.starter.idempotent.aop.Idempotent;
import cn.shanguoyu.base.springboot.starter.idempotent.core.IdempotentParamWrapper;
import cn.shanguoyu.base.springboot.starter.idempotent.core.strategy.IdempotentExecuteHandler;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @description:抽象幂等执行处理器，模板模式
 * @author：sgy
 * @date: 2023-06-03
 */
public abstract class AbstractIdempotentTemplate implements IdempotentExecuteHandler {

    @Override
    public void execute(ProceedingJoinPoint joinPoint, Idempotent idempotent) {
        IdempotentParamWrapper idempotentParamWrapper = IdempotentParamWrapper.builder()
                .joinPoint(joinPoint)
                .idempotent(idempotent)
                .build();
        buildWrapper(idempotentParamWrapper);
        handler(idempotentParamWrapper);
    }

    /**
     * 构建幂等验证过程中所需要的参数包装器
     * 不同的幂等实现方式生成lockkey的方式不一样
     *
     * @return 幂等参数包装器
     */
    protected abstract void buildWrapper(IdempotentParamWrapper idempotentParamWrapper);

    /**
     * 幂等处理逻辑，真正子类实现幂等逻辑的的方法
     *
     * @param wrapper 幂等参数包装器
     */
    protected abstract void handler(IdempotentParamWrapper wrapper);
}
