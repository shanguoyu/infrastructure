package cn.shanguoyu.base.ddd.framework.core.domain;

/**
 * @description:命令处理器
 * @author：sgy
 * @date: 2023-03-09
 */
public interface CommandHandler<T,R> {
    /**
     * 命令执行
     * @param requestParam
     * @return
     */
    R handler(T requestParam);

}
