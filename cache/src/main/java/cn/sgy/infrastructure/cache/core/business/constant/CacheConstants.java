package cn.sgy.infrastructure.cache.core.business.constant;

/**
 * @description 缓存的key的常量
 * @author sgy
 * @date 2023-03-19
 */
public class CacheConstants {


    public static final String JWT_ACCESS_TOKEN = "jwt:access:token:{}";
    public static final String JWT_REFRESH_TOKEN = "jwt:refresh:token:{}";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 登录用户编号 redis key
     */
    public static final String LOGIN_USERID_KEY = "login_userid:";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 登录账户密码错误次数 redis key
     */
    public static final String PWD_ERR_CNT_KEY = "pwd_err_cnt:";

    /**
     * 二维码过期 redis key
     */
    public static final String WX_LOGIN_TOKEN = "wx:login:token:";

    /**
     * 手机号获取短信次数redis key
     */
    public static final String WX_LOGIN_OPENID = "wx:login:openId:";

    /**
     * 微信生成的秘钥redis key
     */
    public static final String WX_USER_SESSION_KEY = "wx:user:sessionKey:";

    /**
     * 保存登录状态redis key
     */
    public static final String WX_USER_USER_ID = "wx:user:userId:";

    /**
     * 保存CC视频 验证码 redis key
     * freetime
     * message
     */
    public static final String CC_VIDEO_VC = "ccvideo:vc:";
}
