/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.sgy.infrastructure.cache.core.business.core;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.sgy.infrastructure.cache.core.business.abs.Cache;
import cn.sgy.infrastructure.cache.core.business.toolkit.CacheUtil;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * @author sgy
 * @description 分布式缓存之操作 Redis 模版代理
 * @date 2023-03-19
 */
@Slf4j
@RequiredArgsConstructor
public class RedisTemplateProxy implements Cache {
    private final RedisTemplate<String, String> myStringRedisTemplate;
    private final RedisTemplate redisTemplate;
    private final RedissonClient redissonClient;
    private final DefaultRedisScript redisPutIfAllAbsentLua;
    private static final String SAFE_GET_DISTRIBUTED_LOCK_KEY_PREFIX = "common:safe-get:lock:";



    /*--------------------------------------------------String的API--------------------------------------------------*/

    @Override
    public <T> T strGet(String key, Class<T> clazz) {
        // 如果是原始类型
        if (ClassUtil.isSimpleValueType(clazz)) {
            String value = myStringRedisTemplate.opsForValue().get(key);
            return Convert.convert(clazz, value);
        }
        // 如果是对象类型
        return (T) redisTemplate.opsForValue().get(key);
    }

    @Override
    public String strGet(String key) {
        return strGet(key, String.class);
    }

    @Override
    public List<String> strGet(Collection<String> keys) {
        return myStringRedisTemplate.opsForValue().multiGet(keys);
    }

    @Override
    public Long strIncr(String key) {
        return myStringRedisTemplate.opsForValue().increment(key);
    }

    @Override
    public Double strIncr(String key, double var) {
        return myStringRedisTemplate.opsForValue().increment(key, var);
    }

    @Override
    public Long strIncr(String key, long var) {
        return myStringRedisTemplate.opsForValue().increment(key, var);
    }

    @Override
    public void strPut(Map<String, Object> map) {
        redisTemplate.opsForValue().multiSet(map);
    }

    @Override
    public void strPut(String key, Object value) {
        if (ObjectUtil.isBasicType(value)) {
            myStringRedisTemplate.opsForValue().set(key, value.toString());
        } else {
            redisTemplate.opsForValue().set(key, value);
        }
    }

    @Override
    public void strPut(String key, Object value, Long exp) {
        strPut(key, value, exp, TimeUnit.SECONDS);
    }

    @Override
    public void strPut(String key, Object value, Long exp, TimeUnit timeUnit) {
        if (ObjectUtil.isBasicType(value)) {
            myStringRedisTemplate.opsForValue().set(key.toString(), value.toString(), exp, timeUnit);
        } else {
            redisTemplate.opsForValue().set(key.toString(), value, exp, timeUnit);
        }
    }

    @Override
    public Boolean strPutIfAbsent(String key, Object value, Long exp, TimeUnit timeUnit) {
        return redisTemplate.opsForValue().setIfAbsent(key, value, exp, timeUnit);
    }

    @Override
    public Boolean strPutIfAllAbsent(Collection<String> keys) {
        return this.<Boolean>executeLua(redisPutIfAllAbsentLua, keys, 30000L);
    }

    @Override
    public <T> T strSafeGet(String key, Class<T> clazz, CacheLoader<T> cacheLoader, long timeout, TimeUnit timeUnit, RBloomFilter<String> bloomFilter, CacheGetFilter<String> cacheGetFilter, CacheGetIfAbsent<String> cacheGetIfAbsent) {
        // 从缓存中获取结果
        T result = strGet(key, clazz);
        // 缓存结果不等于空或空字符串直接返回；通过函数判断是否返回空，为了适配布隆过滤器无法删除的场景；两者都不成立，判断布隆过滤器是否存在，不存在返回空
        /*
         * 条件1：不是NULL并且也不是空字符串 -> 说明没有发生缓存穿透 直接返回结果即可
         * 条件2：把cacheGetFilter包装成Optional，如果不是NULL则调用filter方法进行过滤，如果 cacheGetFilter 为 null 或者过滤后的结果为 null，则返回 false，作为整个表达式的最终结果 -> 省略
         * 条件3：把bloomFilter包装成Optional，不是NULL则调用contains方法（不存在）并且取反，如果 bloomFilter 为 null 或者判断后的结果为 null 则返回 false，作为整个表达式的最终结果 -> 不存在布隆过滤器里面 说明缓存里面从来就没有，发生了缓存穿透，直接返回即可
         */
        if (!CacheUtil.isNullOrBlank(result)
                || Optional.ofNullable(cacheGetFilter).map(each -> each.filter(key)).orElse(false)
                || Optional.ofNullable(bloomFilter).map(each -> !each.contains(key)).orElse(false)) {
            return result;
        }
        // 走到这里说明缓存里面没有，DB里面有，需要进行加载到缓存
        // 上分布式锁防止缓存击穿
        RLock lock = redissonClient.getLock(SAFE_GET_DISTRIBUTED_LOCK_KEY_PREFIX + key);
        // 上锁
        lock.lock();
        try {
            // 双重判定锁，减轻获得分布式锁后线程访问数据库压力
            if (CacheUtil.isNullOrBlank(result = strGet(key, clazz))) {
                // 如果访问 cacheLoader 加载数据为空，执行后置函数操作
                if (CacheUtil.isNullOrBlank(result = loadAndSet(key, cacheLoader, timeout, timeUnit, true, bloomFilter))) {
                    Optional.ofNullable(cacheGetIfAbsent).ifPresent(each -> each.execute(key));
                }
            }
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public void strSafePut(String key, Object value, long timeout, TimeUnit timeUnit, RBloomFilter<String> bloomFilter) {
        // 添加到缓存中
        strPut(key, value, timeout, timeUnit);
        // 添加到布隆过滤器
        bloomFilter.add(key);
    }

    @Override
    public void mapPut(String key, String hashKey, String hashValue) {
        myStringRedisTemplate.opsForHash().put(key, hashKey, hashValue);
    }

    @Override
    public String mapKeyGet(String key, String hashKey) {
        return myStringRedisTemplate.opsForHash().get(key, hashKey).toString();
    }

    @Override
    public void mapKeyDel(String key, String hashKey) {
        myStringRedisTemplate.opsForHash().delete(key, hashKey);
    }

    private <T> T loadAndSet(String key, CacheLoader<T> cacheLoader, long timeout, TimeUnit timeUnit, boolean safeFlag, RBloomFilter<String> bloomFilter) {
        // 调用CacheLoader获得数据
        T result = cacheLoader.load();
        // 如果是NULL或者空字符串则直接返回
        if (CacheUtil.isNullOrBlank(result)) {
            return result;
        }
        // 为true
        if (safeFlag) {
            // 安全的添加
            strSafePut(key, result, timeout, timeUnit, bloomFilter);
        } else {
            strPut(key, result, timeout, timeUnit);
        }
        return result;
    }

    /*--------------------------------------------------其他API--------------------------------------------------*/
    @Override
    public Boolean delete(String key) {
        return redisTemplate.delete(key);
    }

    @Override
    public Long delete(Collection<String> keys) {
        return redisTemplate.delete(keys);
    }

    @Override
    public Long countExistingKeys(@NotNull String... keys) {
        return redisTemplate.countExistingKeys(Lists.newArrayList(keys));
    }

    @Override
    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    @Override
    public List<Object> keys(String pattern) {
        List<Object> keys = new ArrayList<>();
        this.scan(pattern, item -> {
            //符合条件的key
            String key = new String(item, StandardCharsets.UTF_8);
            keys.add(key);
        });
        return keys;
    }

    /**
     * scan 实现
     *
     * @param pattern  表达式
     * @param consumer 对迭代到的key进行操作
     */
    private void scan(String pattern, Consumer<byte[]> consumer) {
        this.redisTemplate.execute((RedisConnection connection) -> {
            try (Cursor<byte[]> cursor =
                         connection.scan(ScanOptions.scanOptions()
                                 .count(Long.MAX_VALUE)
                                 .match(pattern).build())) {
                cursor.forEachRemaining(consumer);
                return null;
            } catch (IOException e) {
                log.error("scan错误", e);
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public List<Object> keysBlock(String pattern) {
        Set<Object> set = redisTemplate.keys(pattern);
        List<Object> list = new ArrayList<>();
        list.addAll(set);
        return list;
    }

    @Override
    public <T> T executeLua(DefaultRedisScript defaultRedisScript, Collection<String> keys, Object... values) {
        return (T) redisTemplate.execute(defaultRedisScript, Lists.newArrayList(keys), values);
    }

    @Override
    public void expire(String key, long timeout, TimeUnit unit) {
        redisTemplate.expire(key,timeout,unit);
    }
}
