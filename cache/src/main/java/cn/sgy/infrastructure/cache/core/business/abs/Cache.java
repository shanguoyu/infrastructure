/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.sgy.infrastructure.cache.core.business.abs;

import cn.sgy.infrastructure.cache.core.business.core.CacheGetFilter;
import cn.sgy.infrastructure.cache.core.business.core.CacheGetIfAbsent;
import cn.sgy.infrastructure.cache.core.business.core.CacheLoader;
import org.redisson.api.RBloomFilter;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @description 缓存接口
 * @author sgy
 * @date 2023-03-19
 */
public interface Cache {
    /*--------------------------------------------------String的API--------------------------------------------------*/
    /**
     * 根据key获取缓存
     * 直接返回泛型对应的类型：
     *      - 如果value是String返回就是String
     *      - 如果Clazz不是基本数据类型那么会用FastJSON2转换为对应类型然后返回
     *      - 如果是基本数据类型也是直接返回
     */
    <T> T strGet(String key, Class<T> clazz);
    /**
     * 获取缓存值是String
     */
    String strGet(String key);
    /**
     * 根据key获取多个
     *
     * @param keys 要查询的key集合
     * @return 集合
     */
    List<String> strGet(Collection<String> keys);
    /**
     * redis 计数器 累加
     *
     * @param key 为累计的key，同一key每次调用则值 +1
     * @return 计数器结果
     */
    Long strIncr(String key);
    /**
     * redis 计数器 累加指定的值
     *
     * @param key 为累计的key加指定的值
     * @return 计数器结果
     */
    Long strIncr(String key,long var);
    /**
     * redis 计数器 累加指定小数
     *
     * @param key 为累计的key，同一key每次调用则值 +1
     * @return 计数器结果
     */
    Double strIncr(String key,double var);
    /**
     * 批量set
     *
     * @param map 键值对
     */
    void strPut(Map<String,Object> map);

    /**
     * 放入缓存
     */
    void strPut(@NotBlank String key, Object value);
    /**
     * 以一种"安全"的方式获取缓存，如查询结果为空，调用 {@link CacheLoader} 加载缓存
     * 通过此方式防止程序中可能出现的：缓存穿透、缓存击穿以及缓存雪崩场景，需要客户端传递布隆过滤器，并通过 {@link CacheGetFilter} 解决布隆过滤器无法删除问题，适用于被外部直接调用的接口
     */
    <T> T strSafeGet(@NotBlank String key, Class<T> clazz, CacheLoader<T> cacheLoader, long timeout, TimeUnit timeUnit,
                  RBloomFilter<String> bloomFilter, CacheGetFilter<String> cacheCheckFilter, CacheGetIfAbsent<String> cacheGetIfAbsent);

    /**
     * 往缓存中写入内容
     *
     * @param key   缓存key
     * @param value 缓存value
     * @param exp   超时时间，单位为秒
     */
    void strPut(@NotBlank String key, Object value, Long exp);

    /**
     * 往缓存中写入内容
     *
     * @param key      缓存key
     * @param value    缓存value
     * @param exp      过期时间
     * @param timeUnit 过期单位
     */
    void strPut(String key, Object value, Long exp, TimeUnit timeUnit);
    /**
     * 放入缓存，自定义超时时间，并将 key 加入步隆过滤器。极大概率通过此方式防止：缓存穿透、缓存击穿、缓存雪崩
     * 通过此方式防止程序中可能出现的：缓存穿透、缓存击穿以及缓存雪崩场景，需要客户端传递布隆过滤器，适用于被外部直接调用的接口
     */
    void strSafePut(@NotBlank String key, Object value, long timeout, TimeUnit timeUnit, RBloomFilter<String> bloomFilter);
    /**
     * 如果 keys 全部不存在，则新增一个默认值，返回 true，反之 false
     */
    Boolean strPutIfAllAbsent(@NotNull Collection<String> keys);

    /*--------------------------------------------------Set的API--------------------------------------------------*/
    /*--------------------------------------------------Hash的API--------------------------------------------------*/

    /**
     * Map格式的添加
     * @param key
     * @param hashKey
     * @param hashValue
     */
    void mapPut(String key,String hashKey,String hashValue);
    /**
     * Map格式再根据key的获取
     * @param key
     */
    String mapKeyGet(String key,String hashKey);
    /**
     * Map格式再根据key的删除
     * @param key
     */
    void mapKeyDel(String key,String hashKey);
    /*--------------------------------------------------ZSet的API--------------------------------------------------*/
    /*--------------------------------------------------BitMap的API--------------------------------------------------*/
    /*--------------------------------------------------HyperLogLog的API--------------------------------------------------*/







    /*--------------------------------------------------删除API--------------------------------------------------*/



    /*--------------------------------------------------添加API--------------------------------------------------*/


    /*--------------------------------------------------其他API--------------------------------------------------*/
    /**
     * 删除缓存
     */
    Boolean delete(@NotBlank String key);

    /**
     * 删除 keys，返回删除数量
     */
    Long delete(@NotNull Collection<String> keys);
    /**
     * 统计指定 key 的存在数量
     */
    Long countExistingKeys(@NotNull String... keys);

    /**
     * 判断 key 是否存在
     */
    Boolean hasKey(@NotBlank String key);

    /**
     * 模糊匹配key
     *
     * @param pattern 模糊key
     * @return 缓存中的数据
     */
    List<Object> keys(String pattern);

    /**
     * 原生阻塞keys 不推荐使用
     *
     * @param pattern 模糊key
     * @return 缓存中的数据
     */
    List<Object> keysBlock(String pattern);

    /**
     * 执行Lua脚本
     */
    <T> T executeLua(DefaultRedisScript defaultRedisScript, Collection<String> keys, Object... values);

    /**
     * 如果为空就set值，返回true
     * 如果存在(不为空)不进行操作，并返回false
     */
    Boolean strPutIfAbsent(String key,Object value,Long exp,TimeUnit timeUnit);

    /**
     * 设置过期时间
     */
    void expire(String key, final long timeout, final TimeUnit unit);
}
