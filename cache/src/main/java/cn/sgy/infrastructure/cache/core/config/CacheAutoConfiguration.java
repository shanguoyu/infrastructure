/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.sgy.infrastructure.cache.core.config;

import cn.sgy.infrastructure.cache.core.business.core.RedisTemplateProxy;
import lombok.AllArgsConstructor;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scripting.support.ResourceScriptSource;

/**
 * @author sgy
 * @description 缓存配置自动装配
 * @date 2024/5/4
 */
@AllArgsConstructor
public class CacheAutoConfiguration {
    private static final String LUA_PUT_IF_ALL_ABSENT_SCRIPT_PATH = "lua/putIfAllAbsent.lua";


    /**
     * 一个lua脚本
     * KEY全部不存在，创建并返回 true，反之返回空
     */
    @Bean
    public DefaultRedisScript<Boolean> redisPutIfAllAbsentLua() {
        DefaultRedisScript<Boolean> defaultRedisScript = new DefaultRedisScript<>();
        defaultRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource(LUA_PUT_IF_ALL_ABSENT_SCRIPT_PATH)));
        defaultRedisScript.setResultType(Boolean.class);
        return defaultRedisScript;
    }


    @Bean("redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        // 创建RedisTemplate
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // 设置连接工厂
        template.setConnectionFactory(factory);
        // value序列化器
        GenericJackson2JsonRedisSerializer jsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        // 设置key序列化
        template.setKeySerializer(RedisSerializer.string());
        // 设置value序列化
        template.setValueSerializer(jsonRedisSerializer);
        // 设置hash key的序列化
        template.setHashKeySerializer(RedisSerializer.string());
        // 设置hash value的序列化
        template.setHashValueSerializer(jsonRedisSerializer);
        return template;
    }

    @Bean(name = "myStringRedisTemplate")
    public RedisTemplate<String, String> myStringRedisTemplate(RedisConnectionFactory factory) {
        StringRedisSerializer serializer = new StringRedisSerializer();
        // 创建RedisTemplate
        RedisTemplate<String, String> template = new RedisTemplate<>();
        // 配置连接工厂
        template.setConnectionFactory(factory);
        // 设置value的序列化方式
        template.setValueSerializer(serializer);
        // 设置key的序列化方式
        template.setKeySerializer(serializer);
        // 设置hash key 和value序列化模式
        template.setHashKeySerializer(serializer);
        template.setHashValueSerializer(serializer);
        return template;
    }

    /**
     * 静态代理模式: Redis 客户端代理类增强
     *
     * @param myStringRedisTemplate
     * @param redisTemplate
     * @param redissonClient
     * @param redisPutIfAllAbsentLua
     * @return
     */
    @Bean
    public RedisTemplateProxy redisTemplateProxy(
            @Qualifier(value = "myStringRedisTemplate")RedisTemplate myStringRedisTemplate,
            RedisTemplate redisTemplate,
            RedissonClient redissonClient,
            @Qualifier(value = "redisPutIfAllAbsentLua") DefaultRedisScript redisPutIfAllAbsentLua) {
        return new RedisTemplateProxy(myStringRedisTemplate, redisTemplate, redissonClient, redisPutIfAllAbsentLua);
    }
}
