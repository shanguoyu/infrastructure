package cn.sgy.infrastructure.file.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * @author sgy
 * @description 实体注释中生成的类型枚举
 * @date 2023-03-09
 */
@Getter
@AllArgsConstructor
public enum DataTypeEnum {

    /**
     * DIR="目录"
     */
    DIR("dir","目录"),
    /**
     * IMAGE="图片"
     */
    IMAGE("image","图片"),
    /**
     * VIDEO="视频"
     */
    VIDEO("video","视频"),
    /**
     * AUDIO="音频"
     */
    AUDIO("audio","音频"),
    /**
     * DOC="文档"
     */
    DOC("doc","文档"),
    /**
     * OTHER="其他"
     */
    OTHER("other","其他"),
    ;

    private String desc1;
    private String desc2;

    public static DataTypeEnum match(String val, DataTypeEnum def) {
        for (DataTypeEnum enm : DataTypeEnum.values()) {
            if (enm.name().equalsIgnoreCase(val)) {
                return enm;
            }
        }
        return def;
    }

    public static DataTypeEnum get(String val) {
        return match(val, null);
    }

    public boolean eq(String val) {
        return this.name().equalsIgnoreCase(val);
    }

    public boolean eq(DataTypeEnum val) {
        if (val == null) {
            return false;
        }
        return eq(val.name());
    }

    public String getCode() {
        return this.name();
    }

}
