package cn.sgy.infrastructure.file.properties;

import cn.sgy.infrastructure.file.enums.FileStorageTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author sgy
 * @description
 * @date 2023-11-17
 */
@ConfigurationProperties(prefix = "infrastructure.file")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileStoreProperties {
    /**
     * 分片文件临时存储目录
     */
    private String chunkStoragePath;
    /**
     * 存储方案
     */
    private FileStorageTypeEnum type;
    /**
     * Oss配置信息
     */
    private OssProperties oss;
    /**
     * MinIO配置信息
     */
    private MinIoProperties minio;
    /**
     * @author sgy
     * @description Oss配置类
     * @date 2023-11-17
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OssProperties {
        /**
         * accessKeyId
         */
        private String accessKeyId;
        /**
         * accessKeySecret
         */
        private String accessKeySecret;
        /**
         * bucketName
         */
        private String bucketName;
        /**
         * endpoint
         */
        private String endpoint;
    }

    /**
     * @author sgy
     * @description Minio配置类
     * @date 2023-03-14
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class MinIoProperties {
        /**
         * 端点，minio地址
         */
        private String endpoint;
//        /**
//         * accessKey
//         */
//        private String accessKey;
//        /**
//         * secretKey
//         */
//        private String secretKey;
        /**
         * bucket
         */
        private String bucket;
        /**
         * ROOT_USER
         **/
        private String user;
        /**
         * ROOT_PASSWORD
         */
        private String password;
    }

}
