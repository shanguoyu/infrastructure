package cn.sgy.infrastructure.file.minio.config;//package cn.shanguoyu.base.file.springboot.starter.minio.config;
//
//import cn.shanguoyu.base.file.springboot.starter.minio.MinioUtil;
//import io.minio.MinioClient;
//import io.minio.MinioProperties;
//import lombok.AllArgsConstructor;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//
///**
// * @description:Minio 自动装配配置
// * @author：sgy
// * @date: 2023-03-14
// */
//@AllArgsConstructor
//@EnableConfigurationProperties(MinioProperties.class)
//public class MinioAutoConfiguration {
//    private final MinioProperties minioProperties;
//
//    @Bean
//    public MinioClient minioClient() {
//        return MinioClient.builder()
//                .endpoint(minioProperties.getEndpoint())
//                .credentials(minioProperties.getUser(), minioProperties.getPassword())
//                .build();
//    }
//
//    @Bean
//    public MinioTemplate minioTemplate(MinioClient minioClient) {
//        return new MinioTemplate(minioClient, minioProperties);
//    }
//
//    @Bean
//    public MinioUtil minioUtil(MinioProperties minioProperties) {
//        return new MinioUtil(minioProperties);
//    }
//}
