package cn.sgy.infrastructure.file.strategy;

import cn.sgy.infrastructure.file.entity.File;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author sgy
 * @description 文件处理策略
 * @date 2023-11-17
 */
public interface FileStore {
    /**
     * 文件上传接口
     */
    File upload(MultipartFile mf);
}
