package cn.sgy.infrastructure.file.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sgy
 * @description File对象
 * @date 2023-11-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class File {
    /**
     * 原始文件名
     */
    private String submittedFileName;
    /**
     * 存储在分布式文件系统的新文件名
     */
    private String fileName;
    /**
     * 文件名后缀
     * (没有.)
     * 可能为NULL
     */
    private String ext;
    /**
     * 文件大小
     * 单位字节
     */
    private Long size;
    /**
     * md5值
     */
    private String fileMd5;
    /**
     * 文件类型
     * 取上传文件的值
     * 可能为NULL
     */
    private String contentType;
    /**
     * 数据类型
     * #DataType{DIR:目录;IMAGE:图片;VIDEO:视频;AUDIO:音频;DOC:文档;OTHER:其他}
     * 可能为NULL
     */
    private String dataType;
    /**
     * 文件访问链接
     * 需要通过nginx配置路由，才能访问，也有可能是OSS的图片外网访问连接
     */
    private String url;

}
