package cn.sgy.infrastructure.file.enums;

/**
 * @description 文件存储类型枚举
 * @author sgy
 * @date: 2023-03-12
 */
public enum FileStorageTypeEnum {
    ALI, MINIO;

    public boolean eq(FileStorageTypeEnum type) {
        for (FileStorageTypeEnum t : FileStorageTypeEnum.values()) {
            return t.equals(type);
        }
        return false;
    }
}
