<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <!-- GAV坐标 -->
    <groupId>cn.sgy.infrastructure</groupId>
    <artifactId>infrastructure-parent</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <!-- 子模块声明 -->
    <modules>
        <module>common</module>
        <module>base</module>
        <module>convention</module>
        <module>web</module>
        <module>db</module>
        <module>cache</module>
        <module>designpattern</module>
        <module>log-alarm</module>
        <module>file</module>
    </modules>

    <!-- 打包方式 -->
    <packaging>pom</packaging>

    <!-- 项目描述 -->
    <description>
        基础组件库
    </description>

    <!-- 开源许可证 -->
    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <!-- 开发者 -->
    <developers>
        <developer>
            <name>sgy</name>
            <email>1259591265@qq.com</email>
            <url></url>
        </developer>
    </developers>

    <!-- 版本声明 -->
    <properties>
        <jdk.version>1.8</jdk.version>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <sourceEncoding>UTF-8</sourceEncoding>
        <spring-boot.version>2.3.12.RELEASE</spring-boot.version>
        <spring-cloud-dependencies.version>Hoxton.SR12</spring-cloud-dependencies.version>
        <spring-cloud-alibaba-dependencies.version>2.2.9.RELEASE</spring-cloud-alibaba-dependencies.version>
        <spring-cloud-starter-openfeign.version>2.2.5.RELEASE</spring-cloud-starter-openfeign.version>
        <spring-cloud-starter-stream-rocketmq.version>2.2.6.RELEASE</spring-cloud-starter-stream-rocketmq.version>
        <spring-cloud-starter-alibaba-sentinel.version>2.2.7.RELEASE</spring-cloud-starter-alibaba-sentinel.version>
        <seata.version>1.5.2</seata.version>
        <hutool-all.version>5.8.2</hutool-all.version>
        <guava.version>31.1-jre</guava.version>
        <javafaker.version>1.0.2</javafaker.version>
        <commons-lang.version>2.4</commons-lang.version>
        <mapstruct.version>1.4.2.Final</mapstruct.version>
        <dozer.version>6.5.2</dozer.version>
        <fastjson2.version>2.0.7</fastjson2.version>
        <hibernate-validator.version>5.4.2.Final</hibernate-validator.version>
        <validation-api.version>2.0.1.Final</validation-api.version>
        <jjwt.version>0.9.1</jjwt.version>
        <okhttp3.version>4.9.3</okhttp3.version>
        <lombok.version>1.18.24</lombok.version>
        <canal-client.version>1.1.6</canal-client.version>
        <xxl-job.version>2.3.1</xxl-job.version>
        <hippo4j-config.version>1.4.3</hippo4j-config.version>
        <minio.verion>8.4.2</minio.verion>
        <shardingsphere.version>5.1.1</shardingsphere.version>
        <redisson-spring-boot-starter.version>3.17.3</redisson-spring-boot-starter.version>
        <knife4j-spring-boot-starter.version>3.0.3</knife4j-spring-boot-starter.version>
        <spotless-maven-plugin.version>2.22.1</spotless-maven-plugin.version>
        <maven-checkstyle-plugin.version>3.1.0</maven-checkstyle-plugin.version>
        <mybatis-plus-boot-starter.version>3.4.2</mybatis-plus-boot-starter.version>
        <antisamy.version>1.7.0</antisamy.version>
        <jjwt.version>0.9.1</jjwt.version>
        <aliyun-sdk-oss.version>3.17.2</aliyun-sdk-oss.version>
        <slf4j.version>1.7.30</slf4j.version>
    </properties>

    <!-- 子模块依赖管理 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
            <dependency>
                <groupId>io.minio</groupId>
                <artifactId>minio</artifactId>
                <version>${minio.verion}</version>
            </dependency>
            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt</artifactId>
                <version>${jjwt.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
                <version>${spring-cloud-alibaba-dependencies.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.springframework.cloud</groupId>
                        <artifactId>spring-cloud-netflix-archaius</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud-dependencies.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring-cloud-alibaba-dependencies.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>commons-lang</groupId>
                <artifactId>commons-lang</artifactId>
                <version>${commons-lang.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-openfeign</artifactId>
                <version>${spring-cloud-starter-openfeign.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.springframework.cloud</groupId>
                        <artifactId>spring-cloud-netflix-archaius</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-starter-stream-rocketmq</artifactId>
                <version>${spring-cloud-starter-stream-rocketmq.version}</version>
            </dependency>

            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
                <version>${spring-cloud-starter-alibaba-sentinel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.mapstruct</groupId>
                <artifactId>mapstruct</artifactId>
                <version>${mapstruct.version}</version>
            </dependency>

            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-validator</artifactId>
                <version>${hibernate-validator.version}</version>
            </dependency>

            <dependency>
                <groupId>javax.validation</groupId>
                <artifactId>validation-api</artifactId>
                <version>${validation-api.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.antisamy</groupId>
                <artifactId>antisamy</artifactId>
                <version>${antisamy.version}</version>
                <exclusions>
                    <exclusion>
                        <artifactId>commons-logging</artifactId>
                        <groupId>commons-logging</groupId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool-all.version}</version>
            </dependency>

            <dependency>
                <groupId>com.alibaba.fastjson2</groupId>
                <artifactId>fastjson2</artifactId>
                <version>${fastjson2.version}</version>
            </dependency>

            <dependency>
                <groupId>org.redisson</groupId>
                <artifactId>redisson-spring-boot-starter</artifactId>
                <version>${redisson-spring-boot-starter.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.shardingsphere</groupId>
                <artifactId>shardingsphere-jdbc-core-spring-boot-starter</artifactId>
                <version>${shardingsphere-jdbc-core-spring-boot-starter.version}</version>
            </dependency>

            <dependency>
                <groupId>com.squareup.okhttp3</groupId>
                <artifactId>okhttp</artifactId>
                <version>${okhttp3.version}</version>
            </dependency>

            <dependency>
                <groupId>com.github.javafaker</groupId>
                <artifactId>javafaker</artifactId>
                <version>${javafaker.version}</version>
            </dependency>

            <dependency>
                <groupId>cn.hippo4j</groupId>
                <artifactId>hippo4j-config-spring-boot-starter</artifactId>
                <version>${hippo4j-config.version}</version>
            </dependency>

            <dependency>
                <groupId>io.seata</groupId>
                <artifactId>seata-spring-boot-starter</artifactId>
                <version>${seata.version}</version>
            </dependency>
            <dependency>
                <groupId>com.aliyun.oss</groupId>
                <artifactId>aliyun-sdk-oss</artifactId>
                <version>${aliyun-sdk-oss.version}</version>
            </dependency>
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis-plus-boot-starter.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!-- 子模块直接继承的依赖 -->
    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
        </dependency>
    </dependencies>

    <!-- 插件 -->
    <build>
        <plugins>
<!--            <plugin>-->
<!--                <groupId>com.diffplug.spotless</groupId>-->
<!--                <artifactId>spotless-maven-plugin</artifactId>-->
<!--                <version>${spotless-maven-plugin.version}</version>-->
<!--                <configuration>-->
<!--                    <java>-->
<!--                        <eclipse>-->
<!--                            <file>${maven.multiModuleProjectDirectory}/dev-support/congomall_spotless_formatter.xml-->
<!--                            </file>-->
<!--                        </eclipse>-->
<!--                        <licenseHeader>-->
<!--                            <file>${maven.multiModuleProjectDirectory}/dev-support/license-header</file>-->
<!--                        </licenseHeader>-->
<!--                    </java>-->
<!--                    &lt;!&ndash;<pom>-->
<!--                        <sortPom>-->
<!--                            <encoding>UTF-8</encoding>-->
<!--                            <nrOfIndentSpace>4</nrOfIndentSpace>-->
<!--                            <keepBlankLines>true</keepBlankLines>-->
<!--                            <indentBlankLines>true</indentBlankLines>-->
<!--                            <indentSchemaLocation>true</indentSchemaLocation>-->
<!--                            <spaceBeforeCloseEmptyElement>true</spaceBeforeCloseEmptyElement>-->
<!--                            <sortModules>false</sortModules>-->
<!--                            <sortExecutions>false</sortExecutions>-->
<!--                            <predefinedSortOrder>custom_1</predefinedSortOrder>-->
<!--                            <expandEmptyElements>false</expandEmptyElements>-->
<!--                            <sortProperties>false</sortProperties>-->
<!--                        </sortPom>-->
<!--                    </pom>&ndash;&gt;-->
<!--                </configuration>-->
<!--                <executions>-->
<!--                    <execution>-->
<!--                        <goals>-->
<!--                            <goal>apply</goal>-->
<!--                        </goals>-->
<!--                        <phase>compile</phase>-->
<!--                    </execution>-->
<!--                </executions>-->
<!--            </plugin>-->

<!--            <plugin>-->
<!--                <artifactId>maven-checkstyle-plugin</artifactId>-->
<!--                <version>${maven-checkstyle-plugin.version}</version>-->
<!--                <configuration>-->
<!--                    <configLocation>${maven.multiModuleProjectDirectory}/dev-support/checkstyle.xml</configLocation>-->
<!--                    <includeTestSourceDirectory>true</includeTestSourceDirectory>-->
<!--                    <excludes>**/autogen/**/*</excludes>-->
<!--                </configuration>-->
<!--                <executions>-->
<!--                    <execution>-->
<!--                        <id>validate</id>-->
<!--                        <goals>-->
<!--                            <goal>check</goal>-->
<!--                        </goals>-->
<!--                        <phase>validate</phase>-->
<!--                    </execution>-->
<!--                </executions>-->
<!--            </plugin>-->

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.5.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </path>
                        <path>
                            <groupId>org.mapstruct</groupId>
                            <artifactId>mapstruct-processor</artifactId>
                            <version>${mapstruct.version}</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>