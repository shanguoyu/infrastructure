package cn.sgy.infrastructure.web.exception.config;

import cn.sgy.infrastructure.web.exception.business.GlobalExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description web异常相关自动装配
 * @author sgy
 * @date 2023-05-28
 */
@Configuration(proxyBeanMethods = false)
public class WebExceptionAutoConfiguration {
    @Bean
    public GlobalExceptionHandler congoMallGlobalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

}
