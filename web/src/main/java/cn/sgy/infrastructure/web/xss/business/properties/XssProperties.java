package cn.sgy.infrastructure.web.xss.business.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author sgy
 * @description xss的属性配置
 * @date 2022/8/20 21:08
 */
@Data
@ConfigurationProperties(prefix = "infrastructure.web.xss")
public class XssProperties {
    /**
     * 是否开启xss防御
     * 默认开启
     * true：开启
     * false：不开启
     *
     */
    private Boolean enable = true;
    /**
     * 排除的路径（不需要XSS过滤的路径）
     * 默认所有路径都需要过滤
     */
    private List<String> exclusionAddr;
}
