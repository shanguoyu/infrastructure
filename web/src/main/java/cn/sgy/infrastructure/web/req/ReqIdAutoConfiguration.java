package cn.sgy.infrastructure.web.req;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author sgy
 * @description 请求唯一id自动配置类
 * @date 2024/9/29
 */
@Configuration(proxyBeanMethods = false)
public class ReqIdAutoConfiguration {
    @Bean
    public FilterRegistrationBean<ReqIdFilter> reqIdGenerateFilter() {
        FilterRegistrationBean<ReqIdFilter> registration = new FilterRegistrationBean();
        registration.setFilter(new ReqIdFilter());
        registration.addUrlPatterns("/*");
        registration.setName("reqIdFilter");
        registration.setOrder(Integer.MIN_VALUE);
        return registration;
    }
}
