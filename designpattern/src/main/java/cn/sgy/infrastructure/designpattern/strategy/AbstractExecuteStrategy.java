package cn.sgy.infrastructure.designpattern.strategy;

/**
 * @description:策略执行抽象
 * @author：sgy
 * @date: 2023-02-28
 */
public interface AbstractExecuteStrategy<REQUEST, RESPONSE> {
    /**
     * 生成策略标识
     */
    String mark();

    /**
     * 执行策略
     *
     * @param requestParam 执行策略入参
     */
    default void execute(REQUEST requestParam) {

    }

    /**
     * 执行策略，有返回值
     *
     * @param requestParam 执行策略入参
     */
    default RESPONSE executeResp(REQUEST requestParam) {
        return null;
    }
}
