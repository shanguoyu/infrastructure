package cn.sgy.infrastructure.common.toolkit;

import java.util.Random;

/**
 * 随机数生成，刮刮卡、用户名生成使用
 *
 * @author glz
 * @since 2022/11/28 14:15
 */
public class RandomUtil {

    /**
     * 实例变量 - 用你自己的变量替换下面的例子
     */
    private int x = 0;

    private static final String[] CODE = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "m", "n", "p", "q", "r", "s", "t", "u", "v",
            "w", "x", "y", "z"};

    private RandomUtil() {

    }
    /**
     * 随机生成指定位数的字符串
     */
    public static String getScratchCardRandom(int digit) {
        StringBuilder sb = new StringBuilder("a");
        for (int i = 0; i < digit; i++) {
            int i1 = new Random().nextInt(24) % 24;
            sb.append(CODE[i1]);
        }
        return sb.toString();
    }

    public static void main(String[] args) {

    }
}
