package cn.sgy.infrastructure.common.enums;

/**
 * @description:实体固定字段枚举
 * @author：sgy
 * @date: 2023-03-09
 */
public enum EntityBaseFieldEnum {
    /**
     * 创建时间
     */
    CREATE_TIME("createTime"),

    /**
     * 修改时间
     */
    UPDATE_TIME("updateTime"),
    /**
     * 修改时间
     */
    DEL_Flag("delFlag");


    private final String fieldName;

    EntityBaseFieldEnum(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

    @Override
    public String toString() {
        return fieldName;
    }
}
