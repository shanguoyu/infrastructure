package cn.sgy.infrastructure.common.enums;

/**
 * @description 删除标记枚举
 * @author sgy
 * @date 2023-03-09
 */
public enum DelEnum {
    /**
     * 正常状态
     */
    NORMAL(0,Boolean.FALSE),

    /**
     * 删除状态
     */
    DELETE(1,Boolean.TRUE);


    private final Integer statusCode;
    private final Boolean statusBoolean;

    DelEnum(Integer statusCode,Boolean statusBoolean) {
        this.statusCode = statusCode;
        this.statusBoolean=statusBoolean;
    }

    public Integer code() {
        return this.statusCode;
    }

    public Boolean getStatusBoolean() {
        return statusBoolean;
    }

    public String strCode() {
        return String.valueOf(this.statusCode);
    }

    @Override
    public String toString() {
        return strCode();
    }
}
