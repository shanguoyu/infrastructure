/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.sgy.infrastructure.convention.error.exception;

import cn.sgy.infrastructure.convention.error.code.IErrorInfo;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sgy
 * @description 抽象项目中三类异常体系，客户端异常、服务端异常以及远程服务调用异常
 * @date 2023-02-28
 */
@Getter
@Setter
public abstract class AbstractException extends RuntimeException {
    /**
     * 错误码和错误信息
     */
    public final IErrorInfo errorInfo;

    public AbstractException(IErrorInfo errorCode) {
        this.errorInfo = errorCode;
    }
}
