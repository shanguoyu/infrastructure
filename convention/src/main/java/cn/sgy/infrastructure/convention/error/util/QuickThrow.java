package cn.sgy.infrastructure.convention.error.util;

/**
 * @description 包装一下抛出异常
 * @author sgy
 * @date 2023-02-28
 */
public class QuickThrow {
    /**
     * 快速抛出异常
     *
     * @param exception 异常
     */
    public static void throwException(RuntimeException exception) {
        throw exception;
    }
}
