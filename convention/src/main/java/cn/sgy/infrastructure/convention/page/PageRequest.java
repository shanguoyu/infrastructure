package cn.sgy.infrastructure.convention.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 分页请求对象，可以理解是防腐层的一种实现，不论底层 ORM 框架，对外分页参数属性不变
 * @author sgy
 * @date 2023-02-28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageRequest {
    /**
     * 当前页
     */
    private Long current;
    /**
     * 每页显示条数
     */
    private Long size;
}
