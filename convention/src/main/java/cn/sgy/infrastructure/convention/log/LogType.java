package cn.sgy.infrastructure.convention.log;

/**
 * @author sgy
 * @description 日志类型
 * @date 2024/9/11
 */
public enum LogType {
    RPC_REQ("RPC调用参数"),
    RPC_RESP("RPC调用返回结果"),
    ERROR("错误"),
    IN_PARAM("入口参数"),
    OUT_RESULT("返回结果"),
    BUSINESS("业务逻辑"),
    THIRD_REQ("第三方调用参数"),
    THIRD_RESP("第三方调用返回结果"),
    TASK("定时任务")
    ;

    public String getMsg() {
        return msg;
    }

    private String msg;

    LogType(String msg) {
        this.msg = msg;
    }
}
