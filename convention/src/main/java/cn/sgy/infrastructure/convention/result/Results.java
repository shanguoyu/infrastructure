/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.sgy.infrastructure.convention.result;


import cn.sgy.infrastructure.convention.error.code.IErrorInfo;
import cn.sgy.infrastructure.convention.error.exception.AbstractException;

/**
 * @author sgy
 * @description 全局返回对象构造器
 * @date 2023-02-28
 */
public final class Results {
    /**
     * 构造成功无数据
     *
     * @return Result
     */
    public static Result<Void> success() {
        return new Result<Void>(IErrorInfo.successCode, null, null, null);
    }

    /**
     * 构造带返回数据的成功响应
     *
     * @param data data
     * @param <T>  泛型
     * @return Result
     */
    public static <T> Result<T> success(T data) {
        return new Result<T>(IErrorInfo.successCode, null, data, null);
    }


    /**
     * 通过AbstractException构建失败响应，但无数据
     */
    public static Result<Void> failure(AbstractException exception) {
        IErrorInfo errorInfo = exception.getErrorInfo();
        String errorCode = errorInfo.code();
        String errorMessage = errorInfo.message();
        return new Result<Void>(errorCode, errorMessage, null, null);
    }

    /**
     * 通过AbstractException构建失败响应，有数据
     */
    public static <T> Result<T> failure(AbstractException exception, T data) {
        IErrorInfo errorInfo = exception.getErrorInfo();
        String errorCode = errorInfo.code();
        String errorMessage = errorInfo.message();
        return new Result<T>(errorCode, errorMessage, data, null);
    }

    /**
     * 通过IErrorInfo构建失败响应，有数据
     */
    public static <T> Result<T> failure(IErrorInfo errorInfo, T data) {
        String errorCode = errorInfo.code();
        String errorMessage = errorInfo.message();
        return new Result<T>(errorCode, errorMessage, data, null);
    }

    /**
     * 通过IErrorInfo构建失败响应，无数据
     */
    public static <T> Result<T> failure(IErrorInfo errorInfo) {
        String errorCode = errorInfo.code();
        String errorMessage = errorInfo.message();
        return new Result<T>(errorCode, errorMessage, null, null);
    }

    /**
     * 通过errCode与errMsg构建失败响应
     */
    public static <T> Result<T> failure(String errorCode, String errorMessage) {
        return new Result<T>(errorCode, errorMessage, null, null);
    }
}
