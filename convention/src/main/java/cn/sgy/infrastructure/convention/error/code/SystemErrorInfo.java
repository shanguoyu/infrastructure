package cn.sgy.infrastructure.convention.error.code;

/**
 * @author sgy
 * @description 系统错误信息：和业务规则无关的错误，也称之为预期外错误
 * @date 2024/11/14
 */
public enum SystemErrorInfo implements IErrorInfo{
    // 通用系统错误
    SYSTEM_ERROR("A_00_001", "系统错误"),
    CONFIG_ERROR("A_00_002", "配置错误"),
    MODEL_ERROR("A_00_003", "模型错误"),
    INVOKE_ERROR("A_00_004", "调用错误"),
    // 设计模式异常
    DESIGN_PATTERN_ERROR("A_01_001", "设计模式异常"),
    STRATEGY_NOT_FOUND_ERROR("A_01_002", "策略未找到"),
    STRATEGY_DUPLICATE_ERROR("A_01_003", "策略标识重复");

    /**
     * 错误码：正常为200，异常为异常状态码
     */
    private final String code;
    /**
     * 错误原因：比如权限不足，账号已存在等
     */
    private final String msg;

    SystemErrorInfo(String code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return msg;
    }
}
