package cn.sgy.infrastructure.convention.error.code;

/**
 * @author sgy
 * @description 平台错误信息
 * @date 2023-02-28
 */
public interface IErrorInfo {
    /**
     * 成功的状态码
     */
    public final String successCode = "200";

    /**
     * 错误码
     */
    String code();

    /**
     * 错误信息
     */
    String message();
}
