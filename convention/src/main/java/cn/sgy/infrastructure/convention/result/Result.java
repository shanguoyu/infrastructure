package cn.sgy.infrastructure.convention.result;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.MDC;

import java.io.Serializable;

/**
 * @author sgy
 * @description 全局返回对象
 * @date 2023-02-28
 */
@Data
@NoArgsConstructor
public class Result<T> implements Serializable {
    /**
     * 返回码
     */
    private String code;

    /**
     * 返回消息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    /**
     * 请求ID
     */
    private String requestId;

    public Result(String code, String message, T data, String requestId) {
        this.code = code;
        this.message = message;
        this.data = data;
        if (requestId == null || "".equals(requestId)) {
            String mdcRequestId = MDC.get("requestId");
            if (mdcRequestId != null && !"".equals(mdcRequestId)) {
                this.requestId = mdcRequestId;
            }
        } else {
            this.requestId = requestId;
        }
    }
}
