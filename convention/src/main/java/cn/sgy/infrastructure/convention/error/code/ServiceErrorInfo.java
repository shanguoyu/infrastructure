package cn.sgy.infrastructure.convention.error.code;

/**
 * @author sgy
 * @description 业务异常，和业务规则相关的错误，也称之为预期内错误
 * @date 2024/9/1
 */
public enum ServiceErrorInfo implements IErrorInfo {
    // 通用的业务异常
    ARGUMENT_VALIDATION_ERROR("B_00_001", "参数异常"),
    CHECK_CAPTCHA_ERROR("B_00_002", "图形验证码不正确"),
    CHECK_CODE_ERROR("B_00_003", "短信验证码不正确"),
    // 会话异常
    INVALID_TOKEN_ERROR("B_01_001","token无效"),
    TOKEN_EXPIRE_ERROR("B_01_002","token过期"),
    TOKEN_FALSIFY_ERROR("B_01_003","token被篡改"),
    // 用户异常
    USER_REGISTER_ERROR("B_02_001", "用户注册错误"),
    USERNAME_EXIST_ERROR("B_02_002", "用户名已存在"),
    USER_EXIST_ERROR("B_02_003", "用户已存在"),
    USER_LOGIN_ERROR("B_02_004", "用户登录错误"),
    USERNAME_PWD_ERROR("B_02_005", "用户名或密码不正确"),
    USER_LOGOUT_ERROR("B_02_006", "用户退出登录错误"),
    USER_NOT_LOGIN_ERROR("B_02_007", "用户还未登录，不能退出登录"),
    // 权限异常
    PERMISSION_DENIED_ERROR("B_03_001","没有权限"),
    INSUFFICIENT_PERMISSIONS_ERROR("B_03_002","权限不足"),
    LOGIN_REQUIRED_ERROR("B_03_003","需要登陆"),
    ;
    /**
     * 错误码：正常为200，异常为异常状态码
     */
    private final String code;
    /**
     * 错误原因：比如权限不足，账号已存在等
     */
    private final String msg;

    ServiceErrorInfo(String code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return msg;
    }
}
